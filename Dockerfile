FROM node:10 as node

WORKDIR /app

COPY ./ /app/

RUN npm install --silent

RUN npm rebuild node-sass

ARG configuration=production

RUN npm run build -- --prod --configuration=$configuration

# Stage 1: Based on nginx, to have only the compiled app, ready for production wiht nginx
FROM nginx:alpine

COPY --from=node /app/dist/frontend-stt-fcob /usr/share/nginx/html

# Stage 2: load nginx to default server to evited routing conflicts
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

# Stage 3: Set this to dyno config and run on heroku
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
