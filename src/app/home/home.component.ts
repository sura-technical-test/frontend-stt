import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UsersService} from "../shared/services/users.service";
import {UsersModel} from "../shared/models/users.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listUsers: UsersModel[] = [];

  constructor(
    private usersService: UsersService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.usersService.getAllUsers().then(res => {
      this.listUsers = res['listUsers'];
    }).catch(err => {
      console.error(err);
    })
  }

  onSignOut() {
    localStorage.clear();
    this.router.navigate(['/login'])
  }
}
