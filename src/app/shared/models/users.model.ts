export class UsersModel {
  id: string;
  fullname: string;
  email: string;
  address:string;
  birthday: string;
  phone: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
}
