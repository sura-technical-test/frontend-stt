import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UsersModel} from "../models/users.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  BACKEND_URL = environment.BACKEND_URL;
  constructor(
    private http: HttpClient
  ) {
  }

  public onLoginMethod(user: UsersModel) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.BACKEND_URL}/auth/signin`, user).toPromise().then(data => {
        localStorage.setItem('ACCESS_TOKEN', data['token'])
        resolve(data);
      }).catch(err => {
        reject(err);
      })
    })
  }

  public onRegisterMethod(user: UsersModel) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.BACKEND_URL}/auth/signup`, user).toPromise().then(data => {
        resolve(data);
      }).catch(err => {
        reject(err);
      })
    })
  }
}
