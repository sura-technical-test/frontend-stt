import { Injectable } from '@angular/core';
import {UsersModel} from "../models/users.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  BACKEND_URL = environment.BACKEND_URL;
  constructor(
    private http: HttpClient
  ) { }

  public getAllUsers() {
    const headers = new HttpHeaders({
      'ACCESS_TOKEN': localStorage.getItem('ACCESS_TOKEN'),
    })
    return new Promise((resolve, reject) => {
      this.http.get(`${this.BACKEND_URL}/users`, ).toPromise().then(data => {
        resolve(data);
      }).catch(err => {
        reject(err);
      })
    })
  }
}
