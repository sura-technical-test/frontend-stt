import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../shared/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  showSuccessMessage: boolean;
  successMessage: string;
  showErrorMessage: boolean;
  errorMessage: string;


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.createLoginForm();
  }

  ngOnInit() {
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      password: ['', [Validators.required]]
    })
  }

  onLogin() {
    this.authService.onLoginMethod(this.loginForm.getRawValue()).then(res => {
      this.showSuccessMessage = true;
      this.successMessage = "Welcome Back.... Redirecting!"
      setTimeout(() => {
        this.showSuccessMessage = false
        this.router.navigate(['/']);
      }, 3000)


    }).catch(err => {
      this.showErrorMessage = true;
      this.errorMessage = err.error.message;
      setTimeout(() => {
        this.showErrorMessage = false
      }, 5000)
    })
  }


}

