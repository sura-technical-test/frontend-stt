import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../shared/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  showSuccessMessage: boolean;
  successMessage: string;
  showErrorMessage: boolean;
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.createRegisterForm();
  }

  ngOnInit() {
  }


  createRegisterForm() {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      fullname: ['', [Validators.required]],
      address: [''],
      birthday: [''],
      phone: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }

  onRegister() {
    this.authService.onRegisterMethod(this.registerForm.getRawValue()).then(res => {

      this.showSuccessMessage = true;
      this.successMessage = "Welcome Back.... Redirecting!"
      setTimeout(() => {
        this.showSuccessMessage = false
        this.router.navigate(['/login']);
      }, 3000)
    }).catch(err => {
      this.showErrorMessage = true;
      this.errorMessage = err.error.message;
      setTimeout(() => {
        this.showErrorMessage = false
      }, 5000)
    })
  }

}
